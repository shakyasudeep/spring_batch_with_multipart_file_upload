/**
 * 
 */
package com.demo.foodtruck.config;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.async.AsyncItemWriter;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import com.demo.foodtruck.dao.PersonDao;
import com.demo.foodtruck.listner.JobCompletionNotificationListener;
import com.demo.foodtruck.listner.StepListener;
import com.demo.foodtruck.model.Person;
import com.demo.foodtruck.processor.PersonItemProcessor;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */
@Slf4j
@Configuration
public class ImportJobConfig {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;


	//	@Autowired private DataSource dataSource;


	@Autowired
	private PersonDao personDao;

	@Bean
	public TaskExecutor taskExecutor() {
		
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(64);
		executor.setMaxPoolSize(100);
		executor.setQueueCapacity(100);
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		executor.setThreadNamePrefix("MultiThreaded-");
		return executor;
		
	}

	@Bean
	@StepScope
	public FlatFileItemReader<Person> importReader(@Value("#{jobParameters[fullPathFileName]}") String pathToFile) {
		
		log.info("Processor current thread. {}",Thread.currentThread().getName());		
		
		FlatFileItemReader<Person> reader = new FlatFileItemReader<>();
		if(!StringUtils.isEmpty(pathToFile)) {
			reader.setResource(new FileSystemResource(pathToFile));
			reader.setLineMapper(new DefaultLineMapper<Person>() {{
				setLineTokenizer(new DelimitedLineTokenizer() {{
					setNames(new String[]{"anzsic06", "Area","year","geo_count","ec_count"});
				}});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
					setTargetType(Person.class);
				}});
			}});
		}
		return reader;
	}

	@Bean
	public PersonItemProcessor processor() {
		return new PersonItemProcessor();
	}
	
	@Bean
	public StepListener stepListener() {
		return new StepListener();
	}
	
	@Bean
    public AsyncItemProcessor<Person, Person> asyncProcessor() {		
		
        AsyncItemProcessor<Person, Person> asyncItemProcessor = new AsyncItemProcessor<>();
        asyncItemProcessor.setDelegate(processor());
        asyncItemProcessor.setTaskExecutor(taskExecutor());

        return asyncItemProcessor;
    }


	/*
	 * @Bean public JdbcBatchItemWriter<Person> writer() {
	 * JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<>();
	 * writer.setItemSqlParameterSourceProvider( new
	 * BeanPropertyItemSqlParameterSourceProvider<Person>()); writer.
	 * setSql("INSERT INTO person (anzsic06, area, year, geo_count, ec_count) VALUES (:anzsic06, :Area, :year, :geo_count, :ec_count)"
	 * ); writer.setDataSource(dataSource); return writer; }
	 */



	@Bean 
	public ItemWriter<Person> writer() {		
		
		log.info("Writer current thread. {}",Thread.currentThread().getName());	

		RepositoryItemWriter<Person> writer = new RepositoryItemWriter<Person>();

		writer.setRepository(personDao); 
		writer.setMethodName("save");

		try { 
			writer.afterPropertiesSet(); 
		} catch (Exception e) {
			e.printStackTrace(); }

		return writer; 
	}
	
	@Bean
    public AsyncItemWriter<Person> asyncWriter() {		
		
        AsyncItemWriter<Person> asyncItemWriter = new AsyncItemWriter<>();
        asyncItemWriter.setDelegate(writer());
        return asyncItemWriter;
        
    }

	// end::readerwriterprocessor[]

	// tag::jobstep[]
	@Bean
	public Job importUserJob(ItemReader<Person> importReader,JobCompletionNotificationListener listener) {
		return jobBuilderFactory
				.get("importUserJob")
				.incrementer(new RunIdIncrementer())
				.listener(listener)
				.flow(step1(importReader))
				.end()
				.build();
	}

	@Bean
	public Step step1(@Qualifier("importReader") ItemReader<Person> importReader) {
		return stepBuilderFactory.get("step1").<Person, Future<Person>>chunk(2500)				
				.reader(importReader)
				.processor(asyncProcessor())
				.writer(asyncWriter())
	//			.listener(stepListener())
				.taskExecutor(taskExecutor())
				.build();
	}

	@Bean
	public TomcatServletWebServerFactory containerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
			@Override
			public void customize(Connector connector) {
				((AbstractHttp11Protocol<?>) connector.getProtocolHandler()).setMaxSwallowSize(-1);
			}
		});
		return factory;
	}

}
