/**
 * 
 */
package com.demo.foodtruck.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.demo.foodtruck.async.AsyncService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */
@Slf4j
@Service
public class JobRunnerService {	
	
	@Autowired
	private AsyncService asyncService;

	static final String tempDir = System.getProperty("java.io.tmpdir");

	public void triggerJob(MultipartFile multipartFile) throws Exception {
		
		log.info("File uploading.");

		File fileToImport = new File(tempDir, multipartFile.getOriginalFilename()); 
		OutputStream outputStream = new FileOutputStream(fileToImport);
		IOUtils.copy(multipartFile.getInputStream(), outputStream);
		outputStream.flush();
		outputStream.close(); 
		
		log.info("File upload finished.");
		
		String fileAbsolutePath = fileToImport.getAbsolutePath();

		log.info("Async job started.");
		
		asyncService.launchBatchJob(fileAbsolutePath);	
		
	}

}
