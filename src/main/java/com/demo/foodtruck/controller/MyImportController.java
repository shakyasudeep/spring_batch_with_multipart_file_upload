/**
 * 
 */
package com.demo.foodtruck.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.demo.foodtruck.service.JobRunnerService;

/**
 * @author Sudeep
 *
 */
@RestController
public class MyImportController {
		
	@Autowired
	private JobRunnerService jobRunnerService;	
	
	static final String tempDir = System.getProperty("java.io.tmpdir");

	@RequestMapping(value="/import/file", method=RequestMethod.POST)
	public String create(@RequestParam("file") MultipartFile multipartFile) throws Exception{	
		
		jobRunnerService.triggerJob(multipartFile);		   

		return "OK";
	}

}
