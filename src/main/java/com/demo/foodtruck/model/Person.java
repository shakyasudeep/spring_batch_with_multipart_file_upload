/**
 * 
 */
package com.demo.foodtruck.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Sudeep
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity
public class Person {

	/**
	 * @param firstName2
	 * @param lastName2
	 */
	/*
	 * public Person(String firstName2, String lastName2) { this.firstName =
	 * firstName2; this.lastName = lastName2; }
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String anzsic06;
	private String Area;
	private String year;
	private String geo_count;
	private String ec_count;
}