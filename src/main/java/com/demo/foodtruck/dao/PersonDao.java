/**
 * 
 */
package com.demo.foodtruck.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.foodtruck.model.Person;

/**
 * @author Sudeep
 *
 */
@Repository
public interface PersonDao extends JpaRepository<Person, Long> {

}
