/**
 * 
 */
package com.demo.foodtruck.processor;

import org.springframework.batch.item.ItemProcessor;

import com.demo.foodtruck.model.Person;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */

//@Slf4j
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

	@Override
	  public Person process(Person person) throws Exception {
		
	//	log.info("Processor current thread. {}",Thread.currentThread().getName());		
		
			/*
			 * final String firstName = person.getFirstName().toUpperCase(); final String
			 * lastName = person.getLastName().toUpperCase(); final Person transformedPerson
			 * = new Person(firstName, lastName);
			 * 
			 * log.info("Converting (" + person + ") into (" + transformedPerson + ")");
			 */
	 
	    return person;
	  }

}
