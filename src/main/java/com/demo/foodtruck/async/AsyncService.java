/**
 * 
 */
package com.demo.foodtruck.async;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */
@Slf4j
@Service
public class AsyncService {	

	@Autowired 
	private JobLauncher jobLauncher;

	@Autowired 
	private Job importUserJob;

	@Async
	public void launchBatchJob(String fileAbsolutePath) throws Exception{

		log.info("Job runner started.");

		//Launch the Batch Job
		JobExecution jobExecution = jobLauncher.run(importUserJob, new JobParametersBuilder()
				.addString("fullPathFileName", fileAbsolutePath)
				.toJobParameters());

		log.info("Job runner finished. {}",jobExecution.getExitStatus());
	}



}
