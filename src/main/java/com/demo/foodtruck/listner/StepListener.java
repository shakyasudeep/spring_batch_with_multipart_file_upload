/**
 * 
 */
package com.demo.foodtruck.listner;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */
@Slf4j
@Component
public class StepListener implements StepExecutionListener {

	@Override
	public void beforeStep(StepExecution stepExecution) {

		log.info("Before Step listening, {}", Thread.currentThread().getName());

	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {

		log.info("After Step listening, {}", Thread.currentThread().getName());

		log.info("Step start time, {}", stepExecution.getStartTime());
		log.info("Read count, {}", stepExecution.getReadCount());
		log.info("Skip count, {}", stepExecution.getSkipCount());
		log.info("Commit count, {}", stepExecution.getCommitCount());
		log.info("Step {} finished!", stepExecution.getStepName());
		log.info("Step end time, {}", stepExecution.getEndTime());
		
		log.info("Step summary, {}", stepExecution.getSummary());

		return null;
	}

}
