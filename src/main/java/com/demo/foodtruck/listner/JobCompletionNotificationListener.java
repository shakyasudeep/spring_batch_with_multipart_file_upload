/**
 * 
 */
package com.demo.foodtruck.listner;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.foodtruck.dao.PersonDao;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sudeep
 *
 */
@Slf4j
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	@Autowired
	PersonDao personDao;

	@Override
	public void afterJob(JobExecution jobExecution) {

		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("!!! JOB FINISHED! Time to verify the results");

//			personDao.findAll().forEach(person -> log.info("Found <" + person + "> in the database."));

			/*
			 * List<Person> results = jdbcTemplate
			 * .query("SELECT first_name, last_name FROM people", new RowMapper<Person>() {
			 * 
			 * @Override public Person mapRow(ResultSet rs, int row) throws SQLException {
			 * return new Person(rs.getString(1), rs.getString(2)); } });
			 * 
			 * for (Person person : results) { log.info("Found <" + person +
			 * "> in the database."); }
			 */

		}
	}

}
